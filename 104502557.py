from lark import Lark, Transformer
import sys

grammer = open('grammer.lark', 'r').read()
parser = Lark(grammer, parser='lalr')
# text = open('test_data/07_1.lsp', 'r').read()

class LspTransformer(Transformer):
    var_table = {}
# Token -> int, bool, str(var_id)
    def boolval(self, items):
        if str(items[0]) == '#t':
            return True
        return False
    def numval(self, items):
        return int(items[0])
# [Feature 2] Print
    def print_num(self, items):
        print(items[0])
    def print_bool(self, items):
        if items[0]:
            print('#t')
        else:
            print('#f')
# [Feature 3] Numerical Operations
    def plus(self, items):
        self.check_type(items, 'int')
        return sum(items)
    def minus(self, items):
        self.check_type(items, 'int')
        return items[0] - items[1]
    def multiply(self, items):
        self.check_type(items, 'int')
        res = 1
        for i in items:
            res *= i
        return res
    def divide(self, items):
        self.check_type(items, 'int')
        return items[0] // items[1]
    def modulus(self, items):
        self.check_type(items, 'int')
        return items[0] % items[1]
    def greater(self, items):
        self.check_type(items, 'int')
        return items[0] > items[1]
    def smaller(self, items):
        self.check_type(items, 'int')
        return items[0] < items[1]
    def equal(self, items):
        self.check_type(items, 'int')
        return items[0] == items[1]
# [Feature 4] Logical Operations
    def and_op(self, items):
        self.check_type(items, 'bool')
        return all(items)
    def or_op(self, items):
        self.check_type(items, 'bool')
        return any(items)
    def not_op(self, items):
        self.check_type(items, 'bool')
        return not items[0]
# [Feature 5] 'if' Expression
    def if_exp(self, items):
        self.check_type([items[0]], 'bool')
        if items[0]:
            return items[1]
        return items[2]
# [Feature 6] Variable Definition
    def var_def(self, items):
        self.var_table[str(items[0])] = items[1]
    def variable(self, items):
        key = str(items[0])
        if key in self.var_table:
            return self.var_table[key]
        raise NameError('Variable "{}" is not defined'.format(key))
# [Bonus Feature 2] Type Checking
    def check_type(self, target, type_name):
        for i in target:
            if type(i).__name__ != type_name:
                raise TypeError('Expect "{}" but got "{}"'.format(type_name, type(i).__name__))

if __name__ == '__main__':
    text = sys.stdin.read()
    # print('===== =====')
    # print(text)
    # print('===== =====')
    try:
        tree = parser.parse(text)
        # print(tree.pretty())
        try:
            LspTransformer().transform(tree)
        except NameError as ne:
            print('Name Error:', ne.args[0])
        except TypeError as te:
            print('Type Error:', te.args[0])
        except:
            print('Transformer error')
    except:
# [Feature 1] Syntax Validation
        print('syntax error')
